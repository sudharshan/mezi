from nlu.bots.restaurant_bot import RestaurantBot

__author__ = 'sudharshan'

"""
Demonstrates conversational abilities
"""

rb = RestaurantBot()
print rb.greet_start()
rb.receive_input()
rb.process_request(rb.current_query)
print rb.greet_end()
