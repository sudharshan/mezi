from flask import Flask
from flask import request, jsonify

import domain_constants as dc

__author__ = 'sudharshan'

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/entities', methods=['POST'])
def get_entities():
    data = request.json
    query = data["query"]
    ents = dc.q.process_query(query)
    print ents
    return jsonify(**ents)


@app.route('/testall', methods=['POST'])
def test_entities():
    data = request.json
    queries = data["queries"]
    resp = []
    for query in queries:
        ents = dc.q.process_query(query)
        resp.append(ents)
    return jsonify(resp)

if __name__ == '__main__':
    app.run(debug=True)
