Mezi Restaurant Bot
================

A simple chatbot prototype for Restaurant booking, can handle entities such as date, time , place and no of guests.

This chatbot depends on [spacy](https://spacy.io/) for tagging and dependency parsing, then determines entities.  


Setup
-----

 sudo sh setup.sh - _sets up a conda environment with spacy,flask etc_

Testing
-------

test.py -- _runs entity extraction on a set of sample queries and spits out the output._

demo.py -- _demonstrates chat functionality of the bot , like following up with questions on missed out information_.


app.py  -- _exposes REST endpoints_ /entities and /testall 

example POST request

POST localhost:5000/entities

{
	"query" : "Can you get us a table at Little Italy for 3 of us tonight"
}

Response:

{
  "datetime": "2016-09-09 21:00:00",
  "location": [ "Little Italy"],
  "no_of_guests": 3
}