__author__ = 'sudharshan'


class RestaurantIntent:
    def __init__(self):
        self.location = None
        self.datetime = None
        self.no_of_guests = None

    def set_intent(self, json):
        self.location = json['location']
        self.datetime = json['datetime']
        self.no_of_guests = json['no_of_guests']

    def is_complete(self):
        if self.location is not None and len(self.location) > 0 and self.datetime is not None and self.no_of_guests > 0:
            return True
        else:
            return False

    def get_incomplete_fields(self):
        for v in vars(self):
            if getattr(self, v) is None:
                yield v

    def confirm(self):
        print "I am glad to inform you that your table is booked at {location} {datetime} for {no_of_guests} of you.".format(
            location=self.location[0], datetime=self.datetime, no_of_guests=self.no_of_guests)
