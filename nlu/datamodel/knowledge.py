from model import RestaurantIntent


__author__ = 'sudharshan'


def load_questions(bot_type):
    if bot_type == 'RESTAURANT':
        questions = {
            'no_of_guests': ["How many of you will be going?", "For how many guests should I reserve the table for?"],
            'location': ["Please tell us where would you like to go?", "Can you please provide the location?"],
            "datetime": ["Can you tell us the time you are looking to book?", "What time would you prefer?"]
        }
        return questions


class KnowledgeBase:
    def __init__(self, bot_type):
        self.greetings = ["How can I help you {USERNAME}"]
        self.greetings_end = ["Hope you like the recommendation. Thank you {USERNAME}"]
        self.bot_type = bot_type
        self.questions = load_questions(bot_type)
        pass

    def get_model(self):
        if self.bot_type == 'RESTAURANT':
            return RestaurantIntent()

    def get_question(self, intent):
        return self.questions[intent][0]
