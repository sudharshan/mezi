from bot import Bot
from nlu.datamodel.knowledge import KnowledgeBase
import domain_constants as dc

__author__ = 'sudharshan'


class RestaurantBot(Bot):
    def __init__(self):
        self.knowledge_base = KnowledgeBase('RESTAURANT')
        self.intent_model = self.knowledge_base.get_model()

    def process_request(self, query):
        entity_json = dc.q.process_query(query)
        print entity_json
        self.intent_model.set_intent(entity_json)
        if self.intent_model.is_complete():
            self.search_recommend(self.intent_model)
        else:
            self.fill_intent(self.intent_model)

    def fill_intent(self, intent_model):
        inc_fields = intent_model.get_incomplete_fields()
        for i in inc_fields:
            self.ask_question(i)
        if self.intent_model.is_complete():
            self.conversation_status = "SEARCH_START"
            self.search_recommend(intent_model)
        else:
            self.conversation_status = "REDIRECT"
            self.redirect_to_human()

    def ask_question(self, intent):
        print self.knowledge_base.get_question(intent)
        print " "
        self.receive_input()
        doc = dc.q.nlp(self.current_query)
        if intent == 'datetime':
            self.update_intent(intent, dc.q.parse_date(doc))
            # print vars(self.intent_model)
        elif intent == 'no_of_guests':
            self.update_intent(intent, dc.q.parse_guests(doc))
            # print vars(self.intent_model)
        else:
            print dc.q.get_location(doc)
            # print vars(self.intent_model)

    def update_intent(self, intent, entity):
        setattr(self.intent_model, intent, entity)

    def search_recommend(self, intent_model):
        # Search and find the booking
        intent_model.confirm()
