from datetime import datetime

import parsedatetime as pdt
import spacy
from spacy.tokens import Span
from spacy.parts_of_speech import NOUN, PROPN

from nlu.ParsedQuery import ParsedQuery

"""
Sudharshan Rajendhiran
Email : sudharshan.cbe@gmail.com
"""

__author__ = 'sudharshan'


class QueryParser:
    def __init__(self):
        self.nlp = spacy.load("en")

    def print_deps(self, doc):
        for tok in doc:
            print(tok.orth_, tok.dep_, tok.tag_, tok.pos_, tok.ent_type_, tok.head.orth_, [t.orth_ for t in tok.lefts],
                  [t.orth_ for t in tok.rights], list(tok.subtree))

    def process_query(self, sent):
        doc = self.nlp(sent)
        nlu_query = ParsedQuery(doc)
        nlu_query.nlu["datetime"] = self.parse_date(doc)
        nlu_query.nlu["no_of_guests"] = self.parse_guests(doc)
        nlu_query.nlu["location"] = self.get_location(doc)
        return nlu_query.nlu

    def get_location(self, doc):
        loc = []
        for propn in self.noun_chunks(doc):
            loc.append(str(doc[propn.start: propn.end]))
        if len(loc) > 0:
            return loc
        else:
            return None

    def parse_date(self, doc):
        def is_datetime(entity):
            if entity.label_ == 'TIME':
                return entity
            if entity.label_ == 'DATE':
                return entity

        date_entities = list(filter((lambda ent: is_datetime(ent)), doc.ents))
        if len(date_entities) > 0:
            datetime_text = ' '.join([x.text for x in date_entities])
            cal = pdt.Calendar()
            now = datetime.now()
            dttime = str(cal.parseDT(datetime_text, now)[0])
            return dttime
        else:
            return None

    def parse_guests(self, doc):
        for sent in doc.sents:
            for token in sent:
                if token.tag_ == 'CD':
                    if token.is_digit and str(token.dep_) == 'pobj':
                        # print token.text, token.tag_ , token.pos_ , token.dep_ , token.like_num
                        return int(token.text)
                    elif token.is_digit and token.dep_ == 'ROOT':
                        return int(token.text)
                    elif len(sent) - 3 >= token.idx and token.nbor(i=1).tag_ == 'IN' and token.nbor(i=2).tag_ == 'PRP':
                        return int(token.text)

    def parse_entities(self, doc):
        nnpbuf = []
        for d in doc:
            if d.tag_ == 'NNP':
                nnpbuf.append(d.idx)
            else:
                if len(nnpbuf) > 0:
                    span = doc[nnpbuf[0]: nnpbuf[len(nnpbuf) - 1]]
                    yield span
                nnpbuf = []

    def noun_chunks_iterator(self, doc):
        labels = ['nsubj', 'dobj', 'nsubjpass', 'pcomp', 'pobj', 'attr', 'root', 'poss']
        np_deps = [doc.vocab.strings[label] for label in labels]
        np_label = doc.vocab.strings['NP']
        datetime_entities = [doc.vocab.strings[label] for label in ['DATE', 'TIME']]
        # noun_entities = [doc.vocab.strings[label] for label in ['ORG', 'GPE']]
        for i, word in enumerate(doc):
            if word.pos == PROPN and word.dep in np_deps:
                if word.ent_type not in datetime_entities:
                    yield word.left_edge.i, word.i + 1, np_label
            elif word.dep_ == 'ROOT' and word.pos == NOUN:
                yield word.left_edge.i, word.i + 1, np_label

    def noun_chunks(self, doc):
        spans = []
        for start, end, label in self.noun_chunks_iterator(doc):
            spans.append(Span(doc, start, end, label=label))
        for span in spans:
            yield span
